import wikipedia_autocomplete
import dash
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State
import dash_html_components as html
import dash_core_components as dcc

# options=[
            # {'label': i, 'value': i} for i in ['Economics','Capitalism']
        # ]

external_stylesheets = [
    'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css'
]
external_scripts = [
     "https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js",
     "https://unpkg.com/semantic-ui-react/dist/umd/semantic-ui-react.min.js",
     "https://unpkg.com/axios/dist/axios.min.js"
]
app = dash.Dash(__name__, 
                external_scripts=external_scripts,
                external_stylesheets=external_stylesheets)

app.layout = html.Div([
    wikipedia_autocomplete.WikipediaAutocomplete(
        id="my-multi-dynamic-dropdown"
    ),
    html.Div(id='dd-output-container')
])


@app.callback(
    Output('dd-output-container', "children"),
    [Input("my-multi-dynamic-dropdown", 'value')]
)
def display_output(value):
    return str(value)


if __name__ == '__main__':
    app.run_server(debug=False)
